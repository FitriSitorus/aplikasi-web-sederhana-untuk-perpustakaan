@extends('layouts.master')

@section('title')
    List Buku
@endsection

@section('content')

<a href="/buku/create" class="btn btn-success btn-sm my-2">Tambah</a>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">judul</th>
        <th scope="col">deskripsi</th>
        <th scope="col">pengarang</th>
        <th scope="col">tahun</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
        @forelse ($buku as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->judul}}</td>
            <td>{{$item->deskripsi}}</td>
            <td>{{$item->pengarang}}</td>
            <td>{{$item->tahun}}</td>
            <td>
                <form action="/buku/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/buku/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                    <a href="/buku/{{$item->id}}/edit" class="btn btn-success btn-sm">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="delete">
            </td>

        </tr>
        @empty
        <tr>
            <td>Tidak ada data Buku</td>
        </tr>
            
        @endforelse

    </tbody>
</table>
@endsection

@push('scripts')
<script>
    Swal.fire({
        title: "Berhasil!",
        text: "Memasangkan script sweet alert",
        icon: "success",
        confirmButtonText: "Cool",
    });
</script>
@endpush