@extends('layouts.master')

@section('title')
    Edit Buku {{$buku->judul}}
@endsection

@section('content')
    <form action="/buku/{{$buku->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="title">Judul Buku</label>
            <input type="text" value="{{$buku->judul}}" class="form-control" name="judul" id="judul" placeholder="Masukkan Title">
            @error('judul')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="body">Deskripsi</label>
            <textarea name="deskripsi"id="deskripsi" class="from-control" cols="30" rows="10">{{$buku->deskripsi}}</textarea>
            @error('deskripsi')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="title">Pengarang</label>
            <input type="text" value="{{$buku->pengarang}}" class="form-control" name="pengarang" id="pengarang" placeholder="Masukkan pengarang">
            @error('pengarang')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="title">Tahun</label>
            <input type="text" value="{{$buku->tahun}}" class="form-control" name="tahun" id="tahun" placeholder="Masukkan Tahun">
            @error('tahun')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>


        
        <button type="submit" class="btn btn-primary">Update Buku</button>
    </form></form>
@endsection