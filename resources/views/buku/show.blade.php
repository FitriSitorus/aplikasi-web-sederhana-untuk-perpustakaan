@extends('layouts.master')

@section('title')
    Detail Buku {{$buku->id}}
@endsection

@section('content')

<h1 class="text-primary" {{$buku->judul}}</h1>
<p>{{$buku->deskripsi}}</p>
<h4>Pengarang: {{$buku->pengarang}}</h4>
<h4>Tahun: {{$buku->tahun}}</h4>
<p>Genre</p>

    @foreach ($genre as $value)
        <li>{{$value->nama}}</li>
    @endforeach
@endsection