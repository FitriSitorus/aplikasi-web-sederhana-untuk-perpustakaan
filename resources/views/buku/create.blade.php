@extends('layouts.master')

@section('title')
    Tambah Buku
@endsection

@section('content')
    <form action="/buku" method="POST">
        @csrf
        <div class="form-group">
            <label for="title">Judul Buku</label>
            <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan Title">
            @error('judul')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="body">Deskripsi</label>
            <textarea name="deskripsi" id="deskripsi" class="form-control"></textarea>
            @error('deskripsi')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="title">Pengarang</label>
            <input type="text" class="form-control" name="pengarang" id="pengarang" placeholder="Masukkan pengarang">
            @error('pengarang')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="title">Tahun</label>
            <input type="text" class="form-control" name="tahun" id="tahun" placeholder="Masukkan Tahun">
            @error('tahun')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>

        

        <button type="submit" class="btn btn-primary">Add Buku</button>
    </form>
@endsection