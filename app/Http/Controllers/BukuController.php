<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class BukuController extends Controller
{
    public function create()
    {
        return view('buku.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'judul' => 'required|unique:buku',
            'deskripsi' => 'required',
            'pengarang' => 'required',
            'tahun'  => 'required'

        ],

        $messages = [
            'judul.required' => 'judul tidak boleh kosong',
            'deskripsi.required' => 'deskripsi tidak boleh kosong',
            'pengarang.required' => 'pengarang tidak boleh kosong',
            'tahun.required' => 'tahun tidak boleh kosong'
        ]);

        DB::table('buku')->insert(
            ['judul' => $request['judul'], 
            'deskripsi' => $request['deskripsi'],
            'pengarang'=>$request['pengarang'],
            'tahun'=>$request['tahun']
            ]
        );

        return redirect('/buku');
    }

    public function index()
    {
        $buku = DB::table('buku')->get();

        return view('buku.index', compact('buku'));

    }

    public function show($id)
    {
        $buku = DB::table('buku')-> where('id', $id)->first();
        $genre = DB::table('genre')->where('buku_id', $id)->get();
        return view('buku.show', compact('buku', 'genre'));
    }

    public function edit($id)
    {
        $buku = DB::table('buku')-> where('id', $id)->first();
        return view('buku.edit', compact('buku'));
    }

    public function update(Request $request, $id)
    {
    $request->validate([
        'judul' => 'required|unique:buku',
        'deskripsi' => 'required',
        'pengarang' => 'required',
        'tahun'  => 'required'

    ],

    $messages = [
        'judul.required' => 'judul tidak boleh kosong',
        'deskripsi.required' => 'deskripsi tidak boleh kosong',
        'pengarang.required' => 'pengarang tidak boleh kosong',
        'tahun.required' => 'tahun tidak boleh kosong'
    ]);
    

    $affected = DB::table('buku')
            ->where('id', $id)
            ->update(
                ['judul' => $request['judul'], 
                'deskripsi' => $request['deskripsi'],
                'pengarang'=>$request['pengarang'],
                'tahun'=>$request['tahun']
                ]
            );

            return redirect('/buku');
    }

    public function destroy($id)
    {
        DB::table('buku')->where('id', '=', $id)->delete();

        return redirect('/buku');

    }

}
